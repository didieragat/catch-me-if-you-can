from setuptools import setup

package_name = 'turtlesim_catch_me_if_you_can'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='agathe',
    maintainer_email='didieragat@eisti.eu',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "controller = turtlesim_catch_me_if_you_can.turtle_controller:main",
            "spawner = turtlesim_catch_me_if_you_can.turtle_spawner:main",
        ],
    },
)
