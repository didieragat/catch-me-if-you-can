from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='turtlesim',
            executable='turtlesim_node',
            name='Nga',
        ),
        Node(
            package='turtlesim_catch_me_if_you_can',
            executable='controller',
            name='controller'
        ),
        Node(
            package='turtlesim_catch_me_if_you_can',
            executable='spawner',
            name='spawner'
        )
    ])