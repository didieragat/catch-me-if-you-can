#!/usr/bin/env python3
import math
import rclpy
from functools import partial
from rclpy.node import Node

#imports from my_robot_interfaces package
from turtlesim.msg import Pose
from geometry_msgs.msg import Twist
from my_robot_interfaces.msg import Turtle
from my_robot_interfaces.msg import TurtleArray
from my_robot_interfaces.srv import CatchTurtle


class TurtleControllerNode(Node):
    ##############################FUNCTION#######################################
    # name : __init__
    # parameters : 
    #        self
    # description : initializes TurtleControllerNode Class
    #############################################################################
    def __init__(self):
        super().__init__("turtle_controller")
        # Creates catch_closest_student_first
        self.declare_parameter("catch_closest_student_first", True)

        self.catch_closest_student_first_ = self.get_parameter(
            "catch_closest_student_first").value
        self.student_to_catch_ = None
        self.pose_ = None
        # Publishes Nga's speed
        self.cmd_vel_publisher_ = self.create_publisher(
            Twist, "turtle1/cmd_vel", 10)
        # Subscribes to Nga's position
        self.pose_subscriber_ = self.create_subscription(
            Pose, "turtle1/pose", self.callback_turtle_pose, 10)
        # Subscribes to the alive_turtles array
        self.alive_turtles_subscriber_ = self.create_subscription(
            TurtleArray, "alive_turtles", self.callback_students_in_classroom, 10)
        # creates timer
        self.control_loop_timer_ = self.create_timer(0.01, self.control_loop)
    #############################################################################

    ##############################FUNCTION#######################################
    # name : callback_turtle_pose
    # author : Agathe DIDIER <didieragat@eisti.eu>
    # parameters : 
    #        self
    #        msg
    # description : gets Nga's position
    #############################################################################
    def callback_turtle_pose(self, msg):
        self.pose_ = msg
    #############################################################################

    ##############################FUNCTION#######################################
    # name : callback_students_in_classroom
    # author : Agathe DIDIER <didieragat@eisti.eu>
    # parameters : 
    #        self
    #        msg
    # description : Defines which student will be caught next
    #############################################################################
    def callback_students_in_classroom(self, msg):
        # if there are students in the classroom
        if len(msg.turtles) > 0:
            # get the names of the students in the classroom
            names=[]
            for turtle in msg.turtles:
                names.append(turtle.name)
            # if one of the students in the group is in the classroom, Nga will catch them first
            if "Agathe" in names:
                self.student_to_catch_=msg.turtles[names.index("Agathe")]
            elif "AlexisDM" in names:
                self.student_to_catch_=msg.turtles[names.index("AlexisDM")]
            elif "Pierre" in names:
                self.student_to_catch_=msg.turtles[names.index("Pierre")]
            # otherwise she will catch the closest sudent first
            else:
                if self.catch_closest_student_first_:
                    closest_student = None
                    closest_student_distance = None
                    # get the distance of all the students from Nga
                    for student in msg.turtles:
                        dist_x = student.x - self.pose_.x
                        dist_y = student.y - self.pose_.y
                        distance = math.sqrt(dist_x*dist_x + dist_y*dist_y)
                        # define the closest one
                        if closest_student == None or distance < closest_student_distance:
                            closest_student = student
                            closest_student_distance = distance
                    self.student_to_catch_ = closest_student
                else:
                    self.student_to_catch_ = msg.turtles[0]
    #############################################################################

    ##############################FUNCTION#######################################
    # name : control_loop
    # parameters : 
    #        self
    # description : controls Nga
    #############################################################################
    def control_loop(self):
        # Exits if there is no student in the classroom
        if self.pose_ == None or self.student_to_catch_ == None:
            return
        # gets distance to the next student to catch
        dist_x = self.student_to_catch_.x - self.pose_.x
        dist_y = self.student_to_catch_.y - self.pose_.y
        distance = math.sqrt(dist_x * dist_x + dist_y * dist_y)

        msg = Twist()

        if distance > 0.5:
            # position
            msg.linear.x = 2*distance

            # orientation
            goal_theta = math.atan2(dist_y, dist_x)
            diff = goal_theta - self.pose_.theta
            if diff > math.pi:
                diff -= 2*math.pi
            elif diff < -math.pi:
                diff += 2*math.pi
            # angle
            msg.angular.z = 6*diff
        else:
            # target reached!
            msg.linear.x = 0.0
            msg.angular.z = 0.0
            # reaches the server when a student gets caught
            self.call_catch_turtle_server(self.student_to_catch_.name)
            self.student_to_catch_ = None

        self.cmd_vel_publisher_.publish(msg)
    #############################################################################


    ##############################FUNCTION#######################################
    # name : call_catch_turtle_server
    # parameters : 
    #        self
    #        student_name
    # description : sends a request to the server to warn that a student was caught
    #############################################################################
    def call_catch_turtle_server(self, student_name):
        client = self.create_client(CatchTurtle, "catch_turtle")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("En attente du serveur...")
        #sends the name of the student who was caught
        request = CatchTurtle.Request()
        request.name = student_name

        future = client.call_async(request)
        future.add_done_callback(
            partial(self.callback_call_catch_turtle, student_name=student_name))
    #############################################################################

    ##############################FUNCTION#######################################
    # name : callback_call_catch_turtle
    # author : Agathe DIDIER <didieragat@eisti.eu>
    # parameters : 
    #        self
    #        future
    #        student_name
    # description : gets the response from the server to the student caught request
    #############################################################################
    def callback_call_catch_turtle(self, future, student_name):
        try:
            response = future.result()
            # warn if there is an error in the request response
            if not response.success:
                self.get_logger().error(str(student_name) + " a echappe au controle de Nga et n'a pas pu etre attrappe")
        #raise exception if there is an error in the request
        except Exception as e:
            self.get_logger().error("Service call failed %r" % (e,))
    #############################################################################


def main(args=None):
    rclpy.init(args=args)
    node = TurtleControllerNode()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
