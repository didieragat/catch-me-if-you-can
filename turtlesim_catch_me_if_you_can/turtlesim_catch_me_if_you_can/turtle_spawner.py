#!/usr/bin/env python3
from functools import partial
import random
import math
import rclpy
from rclpy.node import Node

from turtlesim.srv import Spawn
from turtlesim.srv import Kill
from my_robot_interfaces.msg import Turtle
from my_robot_interfaces.msg import TurtleArray
from my_robot_interfaces.srv import CatchTurtle


#List of students' names
INEM_name = ["Hugo","Octave","Gabriel","Maxence","Axel","Mathias","Pierre","Penelope","Romain","Agathe","Mathis","AlexisDM","Edwin","Charles", "Etienne","Alexis","Florian","Clara","Fabien","Emeric","Sabri" ]


class TurtleSpawner(Node):
	##############################FUNCTION#######################################
	# name : __init__
	# parameters : 
	#		self 
	# description : initializes class TurtleSpawner
	#############################################################################
    def __init__(self):
        super().__init__("turtle_spawner")
        self.declare_parameter("spawn_frequency", 1.0)
        self.declare_parameter("student_name_prefix", "turtle")

        self.spawn_frequency_ = self.get_parameter("spawn_frequency").value
        self.student_name_prefix_ = self.get_parameter(
            "student_name_prefix").value
        self.turtle_counter_ = 0
        self.alive_turtles_ = []
        self.alive_turtles_publisher_ = self.create_publisher(
            TurtleArray, "alive_turtles", 10)
        self.spawn_turtle_timer_ = self.create_timer(
            1.0/self.spawn_frequency_, self.spawn_new_turtle)
        self.catch_turtle_service_ = self.create_service(
            CatchTurtle, "catch_turtle", self.callback_catch_student)
	#############################################################################


	##############################FUNCTION#######################################
	# name : callback_catch_student
	# parameters : 
	#		self
	#		request
	#		response
	# description : Nga will catch the student
	#############################################################################
    def callback_catch_student(self, request, response):
        self.call_kill_server(request.name)
        response.success = True
        return response
	#############################################################################


	##############################FUNCTION#######################################
	# name : publish_students_in_classroom
	# parameters : 
	#		self
	# description : publishes the students already in classroom 
	#############################################################################
    def publish_students_in_classroom(self):
        msg = TurtleArray()
        msg.turtles = self.alive_turtles_
        self.alive_turtles_publisher_.publish(msg)
	#############################################################################


	##############################FUNCTION#######################################
	# name : spawn_new_turtle
	# parameters : 
	#		self
	# description : lets a new student spawn in the room
	#############################################################################
    def spawn_new_turtle(self):
        self.turtle_counter_ += 1
        #Students will only spawn if they're not in the classroom :

        #Get the names of all the students already in the classroom
        student_in_classroom_names=[]
        for i in self.alive_turtles_:
            student_in_classroom_names.append(i.name)
        # Get a random name in the class group
        name = str(random.choice(INEM_name))
        # Get another name if the student is already in the classroom
        while(name in student_in_classroom_names):
            name = str(random.choice(INEM_name))
        # Set random coordinates to the turtle
        x = random.uniform(0.0, 11.0)
        y = random.uniform(0.0, 11.0)
        theta = random.uniform(0.0, 2*math.pi)
        self.call_spawn_server(name, x, y, theta)
	#############################################################################

	##############################FUNCTION#######################################
	# name : call_spawn_server
	# parameters : 
	#		self
	#		student_name
	#		x
	#		y
	#		theta
	# description : sends a request to the server to add the student in the classroom
	#############################################################################
    def call_spawn_server(self, student_name, x, y, theta):
        client = self.create_client(Spawn, "spawn")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("En attente du serveur...")

        request = Spawn.Request()
        request.x = x
        request.y = y
        request.theta = theta
        request.name = student_name

        future = client.call_async(request)
        future.add_done_callback(
            partial(self.callback_call_spawn, student_name=student_name, x=x, y=y, theta=theta))
	#############################################################################

	##############################FUNCTION#######################################
	# name : callback_call_spawn
	# parameters : 
	#		self
	#		future
	#		student_name
	#		x
	#		y
	#		theta
	# description : gets the response of the server to the spawn request
	#############################################################################
    def callback_call_spawn(self, future, student_name, x, y, theta):
        try:
            response = future.result()
            if response.name != "":
                self.get_logger().info(response.name + " vient de faire son entree dans la classe !")
                new_student = Turtle()
                new_student.name = response.name
                new_student.x = x
                new_student.y = y
                new_student.theta = theta
                self.alive_turtles_.append(new_student)
                self.publish_students_in_classroom()
        except Exception as e:
            self.get_logger().error("Service call failed %r" % (e,))
	#############################################################################

	##############################FUNCTION#######################################
	# name : call_kill_server
	# parameters : 
	#		self
	#		student_name
	# description : sends a kill request to the server
	#############################################################################
    def call_kill_server(self, student_name):
        client = self.create_client(Kill, "kill")
        while not client.wait_for_service(1.0):
            self.get_logger().warn("En attente du serveur...")

        request = Kill.Request()
        request.name = student_name

        future = client.call_async(request)
        future.add_done_callback(
            partial(self.callback_call_kill, student_name=student_name))
	#############################################################################

	##############################FUNCTION#######################################
	# name : callback_call_kill
	# parameters : 
	#		self
	#		future
	#		student_name
	# description : gets the response of the server to the kill request
	#############################################################################
    def callback_call_kill(self, future, student_name):
        try:
            future.result()
            for (i, turtle) in enumerate(self.alive_turtles_):
                if turtle.name == student_name:
                    self.get_logger().info("Nga a attrape "+turtle.name)
                    del self.alive_turtles_[i]
                    self.publish_students_in_classroom()
                    break
        except Exception as e:
            self.get_logger().error("Service call failed %r" % (e,))
	#############################################################################


def main(args=None):
    rclpy.init(args=args)
    node = TurtleSpawner()
    rclpy.spin(node)
    rclpy.shutdown()


if __name__ == "__main__":
    main()
